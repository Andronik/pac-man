;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Jeu vidéo en assembleur NES basé sur Pac-Man de NAMCO.
;	Création: Andronikos Karkaselis, 14 Novembre 2019
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;; Entète de la rom ;;;;;;;;;;;
	.inesprg 1 ; 1 banque de 16KB (2 banque de 8KB) pour le programme
	.ineschr 1 ; 1 banque de 8KB pour les image (2 tables de motifs)
	.inesmap 0 ; mapper 0 = NROM, Banque par défaut
	.inesmir 1 ; Mirroir vertical de l'image de fond

;;;;;;; Le code commence ici ;;;;;;;;
	.code

	.bank 0   ; Banque 0 (8KB de $C000 à $DFFF)
	.org $C000  ; goto location $C000.


Start:
	SEI         ; Arreter les interruption, Met le bit I du registre P (etat du processeur) a 1
	CLD         ; Arrete le mode decimal. Met le bit D du registre P à 0
	LDX #$40
	STX $4017  ;  Met $40 dans le registre de controle de l'APU pour arreter les interruptions de l'APU
	LDX #$FF
	TXS         ; Initialise la pile. Place X ($FF$ comme octet moins significatif de l'adresse de la pile). La pile est toujours entre $0100 et $01FF.
	INX         ; Lorsqu'on incremente X qui contient $FF, C tombe à 0
	STX $2000  ;  Arrete les interruption NMI (Bit 7 du registre Contrôle PPU)
	STX $2001  ;  N'affiche rien (Voir bits 3 à 7 du registre Masque PPU)
	STX $4010  ;  Arrete les interruption logiciel

	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

clrmem:
	LDA #$00
	STA $0000, x		; Place tous les octets à 0 (", x" correspond a l'adressage indexe avec le registre x)
	STA $0100, x
	STA $0300, x
	STA $0400, x
	STA $0500, x
	STA $0600, x
	STA $0700, x
	LDA #$FE
	STA $0200, x		; Placer tous les sprite en dehors de l'écran
	INX
	BNE clrmem		; Branche si non zero (lorsque x a fait le tour des valeurs de $00 à $FF)

	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

	LDA #4
	STA DirectionPacman
	LDA #1
	STA BouchePacman
	LDA #0
	STA BoucheTimer
	LDA #15
	STA BoucheVitesse
	LDA #1
	STA DirectionFantome1
	LDA #2
	STA DirectionFantome2
	LDA #3
	STA DirectionFantome3
	LDA #4
	STA DirectionFantome4
	LDA #10
	STA SelectionFantome

;;;;  Initialise le PPU  ;;;;
LoadName:
	LDA #$20	; Nous utiliserons la table de nom 0 (Adr $2000)
	STA $2006
	LDA #$00
	STA $2006
	LDX #0	; x sera notre index de largeur
	LDY #0	; y sera notre index de hauteur

LoadPalettes:
 	LDA $2002    		; Lire le registre d'etat du PPU ou annuler le latch de $2006 (s'il y a lieu)
	LDA #$3F	
	STA $2006    		; Place les 8 bits les plus significatifs de l'adresse $3F00 dans $2006
	LDA #$00
	STA $2006    		; Place les 8 bits les moins significatifs de l'adresse $3F00 dans $2006
	LDX #0
LoadPalettesLoop:
	LDA Palette, x		; Charge les donnees de la palette (", x" correspond a l'adressage indexe avec le registre x)
	STA $2007		; Place les informations du PPU (l'adresse d'écriture de $2007 incrémente automatiquement de 1 octet après chaque ecriture)
	INX			; Prochain index a aller chercher dans la palette en memoire rom (etiquette palette+x)
	CPX #32            
	BNE LoadPalettesLoop 	; La palette est completement copiees si x est à 32=16x2

; Initialisation des sprites
	LDX #0
LoadSpritesData:
	LDA SpriteData, x	; SpriteData est en ROM (lecture seule). Pour pouvoir modifier
	STA $0200, x		; ces sprites, nous transférons les données des sprites en RAM
	INX
	CPX #80		; il y a 16 sprites de 4 octets (donc 64 octets) à transférer en RAM
	BNE LoadSpritesData

;;;;  Fin de l'initialisation de données  ;;;;

	JSR vblankwait	; Place l'adresse en cour sur la pile et branche vers la routine "vblankwait"

	JSR activePPU
	
Forever:
	JMP Forever

NMI:

	LDA #$01	; Écrire $01 et $00 dans $4016 place l'état du controlleur 1 dans $4016 et du controlleur 2 dans $4017
	STA $4016
	LDA #$00
	STA $4016	

	LDA $4016	
	AND #1
	BEQ skip
	LDA #0
	STA BoucheRapide
	JMP Done
skip:
	LDA #1
	STA BoucheRapide
Done:
	LDA $4016
	LDA $4016
	LDA $4016
	LDA $4016
	AND #1
	BNE PacmanUpPress
	LDA $4016
	AND #1
	BNE PacmanDownPress
	LDA $4016
	AND #1
	BNE	PacmanLeftPress
	LDA $4016
	AND #1
	BNE	PacmanRightPress
	JMP Direction

PacmanUpPress:
	LDA #1
	STA DirectionPacman
	JMP Direction
PacmanDownPress:
	LDA #2
	STA DirectionPacman
	JMP Direction
PacmanLeftPress:
	LDA #3
	STA DirectionPacman
	JMP Direction
PacmanRightPress:
	LDA #4
	STA DirectionPacman
	JMP Direction
	LDA FantomeOnOff

Direction:
	LDA DirectionPacman
	CMP #1
	BEQ PacmanUpSprite
	CMP #2
	BEQ PacmanDownSprite
	CMP #3
	BEQ PacmanLeftSprite
	CMP #4
	BEQ PacmanRightSpriteR

PacmanUpSprite:
	LDA BouchePacman
	CMP #1
	BNE PacmanBoucheFermeeR
PacmanUpBoucheOuverte:
	LDA #4
	STA $201
	STA $205
	LDA #%00000000
	STA $202
	LDA #%01000000
	STA $206
	LDA #0
	STA $209
	STA $20D
	LDA #%10000000
	STA $20A
	LDA #%11000000
	STA $20E
	JMP PacmanUp

PacmanDownSprite:
	LDA BouchePacman
	CMP #1
	BNE PacmanBoucheFermeeR
PacmanDownBoucheOuverte:
	LDA #0
	STA $201
	STA $205
	LDA #%00000000
	STA $202
	LDA #%01000000
	STA $206
	LDA #4
	STA $209
	STA $20D
	LDA #%10000000
	STA $20A
	LDA #%11000000
	STA $20E
	JMP PacmanDown

PacmanBoucheFermeeR:	; Relais PacmanBoucheFermee
	JMP PacmanBoucheFermee
PacmanRightSpriteR:		; Relais PacmanRightSprite
	JMP PacmanRightSprite

PacmanLeftSprite:
	LDA BouchePacman
	CMP #1
	BNE PacmanBoucheFermee
PacmanLeftBoucheOuverte:
	LDA #1
	STA $201
	STA $209
	LDA #0
	STA $205
	STA $20D
	LDA #%01000000
	STA $202
	STA $206
	LDA #%11000000
	STA $20A
	STA $20E
	JMP PacmanLeft

PacmanRightSprite:
	LDA BouchePacman
	CMP #1
	BNE PacmanBoucheFermee
PacmanRightBoucheOuverte:
	LDA #0
	STA $201
	STA $209
	LDA #1
	STA $205
	STA $20D
	LDA #%00000000
	STA $202
	STA $206
	LDA #%10000000
	STA $20A
	STA $20E
	JMP PacmanRight

PacmanBoucheFermee:
	LDA #0
	STA $201
	STA $205
	STA $209
	STA $20D
	LDA #%00000000
	STA $202
	LDA #%01000000
	STA $206
	LDA #%10000000
	STA $20A
	LDA #%11000000
	STA $20E

CheckDirection:
	LDA DirectionPacman
	CMP #1
	BEQ PacmanUp
	CMP #2
	BEQ PacmanDown
	CMP #3
	BEQ PacmanLeft
	CMP #4
	BEQ PacmanRight

PacmanUp:
	DEC $200
	DEC $204
	DEC $208
	DEC $20C
	LDA #0
	CMP BoucheRapide
	BNE PacmanUpNext
	DEC $200	
	DEC $204
	DEC $208
	DEC $20C
PacmanUpNext:
	LDA #1
	STA DirectionPacman
	JMP FinPacman

PacmanDown:
	INC $200
	INC $204
	INC $208
	INC $20C
	LDA #0
	CMP BoucheRapide
	BNE PacmanDownNext
	INC $200
	INC $204
	INC $208
	INC $20C
PacmanDownNext:
	LDA #2
	STA DirectionPacman
	JMP FinPacman

PacmanLeft:
	DEC $203
	DEC $207
	DEC $20B
	DEC $20F
	LDA #0
	CMP BoucheRapide
	BNE PacmanLeftNext
	DEC $203
	DEC $207
	DEC $20B
	DEC $20F
PacmanLeftNext:
	LDA #3
	STA DirectionPacman
	JMP FinPacman

PacmanRight:
	INC $203
	INC $207
	INC $20B
	INC $20F
	LDA #0
	CMP BoucheRapide
	BNE PacmanRightNext
	INC $203
	INC $207
	INC $20B
	INC $20F
PacmanRightNext:
	LDA #4
	STA DirectionPacman

FinPacman:

	LDA #0
	CMP BoucheRapide
	BEQ TurboMouth
	LDA #15
	STA BoucheVitesse
	JMP Continue
TurboMouth:
	LDA #5
	STA BoucheVitesse
Continue:
	LDA BouchePacman
	CMP #0
	BNE BoucheTimerUP
	JMP BoucheTimerDown
BoucheTimerUP:
	INC BoucheTimer
	JMP CheckTimer
BoucheTimerDown:
	DEC BoucheTimer
	JMP CheckTimer
CheckTimer:
	LDA BoucheTimer
	CMP #0
	BPL BoucheOpen	; previously BEQ
	CMP BoucheVitesse
	BMI BoucheClose ; previously BEQ
	JMP FinTimer
BoucheClose:
	LDA #0
	STA BouchePacman
	JMP FinTimer
BoucheOpen:
	LDA #1
	STA BouchePacman
FinTimer:

	LDA FantomeOnOff
	CMP #1
	BNE	FantomeON
	LDA #5
	STA $211	; Fantome 1
	STA $215
;	STA $219
;	STA $21D
	STA $221	; Fantome 2
	STA $225
;	STA $229
;	STA $22D
	STA $231	; Fantome 3
	STA $235
;	STA $239
;	STA $23D
	STA $241	; Fantome 4
	STA $245
;	STA $249
;	STA $24D
	LDA #0	
	STA FantomeOnOff
	JMP FantomeOnOffEnd
FantomeON:
	LDA #2
	STA $211
	STA $215
	STA $221
	STA $225
	STA $231
	STA $235
	STA $241
	STA $245
	LDA #3
	STA $219
	STA $21D
	STA $229
	STA $22D
	STA $239
	STA $23D
	STA $249
	STA $24D
	LDA #1
	STA FantomeOnOff
FantomeOnOffEnd:

Fantomes:	
	LDA $4017
	AND #1
	BNE Fantome1
	LDA $4017
	AND #1
	BNE Fantome2
	LDA $4017
	AND #1
	BNE Fantome3
	LDA $4017
	AND #1
	BNE Fantome4
	LDA $4017
	AND #1
	BNE FantomeUpPress
	LDA $4017
	AND #1
	BNE FantomeDownPress
	LDA $4017
	AND #1
	BNE	FantomeLeftPressR
	LDA $4017
	AND #1
	BNE	FantomeRightPressR
	JMP DirectionFantomesCheck

Fantome1:
	LDA #10
	STA SelectionFantome
	JMP	DirectionFantomesCheck
Fantome2:
	LDA #20
	STA SelectionFantome
	JMP DirectionFantomesCheck
Fantome3:
	LDA #30
	STA SelectionFantome
	JMP DirectionFantomesCheck
Fantome4:
	LDA #40
	STA SelectionFantome
	JMP	DirectionFantomesCheck

FantomeUpPress:
	LDA SelectionFantome
	CMP #10
	BEQ Fantome1_up
	CMP #20
	BEQ	Fantome2_up
	CMP #30
	BEQ	Fantome3_up
	CMP #40
	BEQ Fantome4_up
	JMP DirectionFantomesCheck

Fantome1_up:
	LDA #1
	STA DirectionFantome1
	JMP DirectionFantomesCheck
Fantome2_up:
	LDA #1
	STA DirectionFantome2
	JMP DirectionFantomesCheck
Fantome3_up:
	LDA #1
	STA DirectionFantome3
	JMP DirectionFantomesCheck
Fantome4_up:
	LDA #1
	STA DirectionFantome4
	JMP DirectionFantomesCheck

FantomeLeftPressR:
	JMP FantomeLeftPress
FantomeRightPressR:
	JMP FantomeRightPress

FantomeDownPress:
	LDA SelectionFantome
	CMP #10
	BEQ Fantome1_down
	CMP #20
	BEQ	Fantome2_down
	CMP #30
	BEQ	Fantome3_down
	CMP #40
	BEQ Fantome4_down
	JMP DirectionFantomesCheck

Fantome1_down:
	LDA #2
	STA DirectionFantome1
	JMP DirectionFantomesCheck
Fantome2_down:
	LDA #2
	STA DirectionFantome2
	JMP DirectionFantomesCheck
Fantome3_down:
	LDA #2
	STA DirectionFantome3
	JMP DirectionFantomesCheck
Fantome4_down:
	LDA #2
	STA DirectionFantome4
	JMP DirectionFantomesCheck

FantomeLeftPress:
	LDA SelectionFantome
	CMP #10
	BEQ Fantome1_left
	CMP #20
	BEQ	Fantome2_left
	CMP #30
	BEQ	Fantome3_left
	CMP #40
	BEQ Fantome4_left
	JMP DirectionFantomesCheck

Fantome1_left:
	LDA #3
	STA DirectionFantome1
	JMP DirectionFantomesCheck
Fantome2_left:
	LDA #3
	STA DirectionFantome2
	JMP DirectionFantomesCheck
Fantome3_left:
	LDA #3
	STA DirectionFantome3
	JMP DirectionFantomesCheck
Fantome4_left:
	LDA #3
	STA DirectionFantome4
	JMP DirectionFantomesCheck

FantomeRightPress:
	LDA SelectionFantome
	CMP #10
	BEQ Fantome1_right
	CMP #20
	BEQ	Fantome2_right
	CMP #30
	BEQ	Fantome3_right
	CMP #40
	BEQ Fantome4_right
	JMP DirectionFantomesCheck

Fantome1_right:
	LDA #4
	STA DirectionFantome1
	JMP DirectionFantomesCheck
Fantome2_right:
	LDA #4
	STA DirectionFantome2
	JMP DirectionFantomesCheck
Fantome3_right:
	LDA #4
	STA DirectionFantome3
	JMP DirectionFantomesCheck
Fantome4_right:
	LDA #4
	STA DirectionFantome4
	JMP DirectionFantomesCheck

DirectionFantomesCheck:
DirectionFantome1Check:
	LDA DirectionFantome1
	CMP #1
	BEQ DirectionFantome_1_up
	CMP #2
	BEQ DirectionFantome_1_down
	CMP #3
	BEQ DirectionFantome_1_left
	CMP #4
	BEQ DirectionFantome_1_right

DirectionFantome_1_up:
	DEC $210
	DEC $214
	DEC $218
	DEC $21C
	JMP finFantome1
DirectionFantome_1_down:
	INC $210
	INC $214
	INC $218
	INC $21C
	JMP finFantome1
DirectionFantome_1_left:
	DEC $213
	DEC $217
	DEC $21B
	DEC $21F
	JMP finFantome1
DirectionFantome_1_right:
	INC $213
	INC $217
	INC $21B
	INC $21F
finFantome1:

DirectionFantome2Check:
	LDA DirectionFantome2
	CMP #1
	BEQ DirectionFantome_2_up
	CMP #2
	BEQ DirectionFantome_2_down
	CMP #3
	BEQ DirectionFantome_2_left
	CMP #4
	BEQ DirectionFantome_2_right

DirectionFantome_2_up:
	DEC $220
	DEC $224
	DEC $228
	DEC $22C
	JMP finFantome2
DirectionFantome_2_down:
	INC $220
	INC $224
	INC $228
	INC $22C
	JMP finFantome2
DirectionFantome_2_left:
	DEC $223
	DEC $227
	DEC $22B
	DEC $22F
	JMP finFantome2
DirectionFantome_2_right:
	INC $223
	INC $227
	INC $22B
	INC $22F
finFantome2:

DirectionFantome3Check:
	LDA DirectionFantome3
	CMP #1
	BEQ DirectionFantome_3_up
	CMP #2
	BEQ DirectionFantome_3_down
	CMP #3
	BEQ DirectionFantome_3_left
	CMP #4
	BEQ DirectionFantome_3_right

DirectionFantome_3_up:
	DEC $230
	DEC $234
	DEC $238
	DEC $23C
	JMP finFantome3
DirectionFantome_3_down:
	INC $230
	INC $234
	INC $238
	INC $23C
	JMP finFantome3
DirectionFantome_3_left:
	DEC $233
	DEC $237
	DEC $23B
	DEC $23F
	JMP finFantome3
DirectionFantome_3_right:
	INC $233
	INC $237
	INC $23B
	INC $23F
finFantome3:

DirectionFantome4Check:
	LDA DirectionFantome4
	CMP #1
	BEQ DirectionFantome_4_up
	CMP #2
	BEQ DirectionFantome_4_down
	CMP #3
	BEQ DirectionFantome_4_left
	CMP #4
	BEQ DirectionFantome_4_right

DirectionFantome_4_up:
	DEC $240
	DEC $244
	DEC $248
	DEC $24C
	JMP finFantome4
DirectionFantome_4_down:
	INC $240
	INC $244
	INC $248
	INC $24C
	JMP finFantome4
DirectionFantome_4_left:
	DEC $243
	DEC $247
	DEC $24B
	DEC $24F
	JMP finFantome4
DirectionFantome_4_right:
	INC $243
	INC $247
	INC $24B
	INC $24F
finFantome4:

VisibilityCheck:
	LDA SelectionFantome
	CMP #10
	BEQ VisibleFantome1
	CMP #20
	BEQ VisibleFantome2
	CMP #30
	BEQ VisibleFantome3
	CMP #40
	BEQ VisibleFantome4

VisibleFantome1:
	LDA #2
	STA $211
	STA $215
	LDA #3
	STA $219
	STA $21D
	JMP finVisibilityCheck
VisibleFantome2:
	LDA #2
	STA $221
	STA $225
	LDA #3
	STA $229
	STA $22D
	JMP finVisibilityCheck
VisibleFantome3:
	LDA #2
	STA $231
	STA $235
	LDA #3
	STA $239
	STA $23D
	JMP finVisibilityCheck
VisibleFantome4:
	LDA #2
	STA $241
	STA $245
	LDA #3
	STA $249
	STA $24D
finVisibilityCheck:

updateSprite:	
	LDA #$00
	STA $2003
	LDA #$02
	STA $4014

	RTI             ; retourne de l'interruption
	
activePPU:
	LDA #%10010000		; Active les interruption NMI, table de motif: sprite = 0 et image de fond = 1
	STA $2000
	LDA #%11111110		; Active l'image de fond et les sprites
	STA $2001
	RTS

vblankwait:			; Routine qui attend que l'écran ait terminé de s'afficher (pour éviter les problèmes visuels)
	BIT $2002		; Bit place les Code de condition N, V, Z.
	BPL vblankwait	; Si le bit 7 est allumé, on a un vblank (BPL = N flag clear, le bit de negatif = bit 7)
	RTS				; Effectue un branchement vers l'adresse sur le dessus de la pile (Voir "JSR vblankwait")



;;;;; La prochaine section est une partie du ROM de la cartouche il est possible de mettre du code ici. Nous allons l'utiliser pour mettre des constantes ;;;;;;

	.bank 1		; Banque 1 (8KB de $E000 à $FFFF)
	.org $E000	; Donnees en lecture seulement

Palette:
	.db $02,$0C,$01,$02, $02,$0C,$01,$02, $02,$0C,$01,$02, $02,$0C,$01,$02	; Palette de l'image de fond
	.db $0D,$28,$20,$0D, $0D,$15,$20,$0D, $0D,$19,$20,$0D, $0D,$11,$20,$0D	; Palette des sprites    	;; $0D,$28,$1D,$18 old

SpriteData:
;        Y  tile   Att      x   				  Y   tile   Att    X
	.db $60, 0, %00000000, $10	; Sprite Pacman $200  $201  $202  $203
	.db $60, 1, %00000000, $18	; Sprite Pacman $204  $205  $206  $207
	.db $68, 0, %10000000, $10	; Sprite Pacman $208  $209  $20A  $20B
	.db $68, 1, %10000000, $18	; Sprite Pacman $20C  $20D  $20E  $20F

	.db $40, 2, %00000001, $80	; Sprt Fantome1 $210  $211  $212  $213
	.db $40, 2, %01000001, $88	; Sprt Fantome1 $214  $215  $216  $217
	.db $48, 3, %00000001, $80	; Sprt Fantome1 $218  $219  $21A  $21B
	.db $48, 3, %01000001, $88	; Sprt Fantome1 $21C  $21D  $21E  $21F

	.db $80, 2, %00000010, $60	; Sprt Fantome2 $220  $221  $222  $223
	.db $80, 2, %01000010, $68	
	.db $88, 3, %00000010, $60	 
	.db $88, 3, %01000010, $68	

	.db $A0, 2, %00000011, $A0	; Sprt Fantome3 $230  $231  $232  $233
	.db $A0, 2, %01000011, $A8	
	.db $A8, 3, %00000011, $A0	 
	.db $A8, 3, %01000011, $A8	

	.db $80, 2, %00000000, $10	; Sprt Fantome4 $240  $241  $242  $243
	.db $80, 2, %01000000, $18	
	.db $88, 3, %00000000, $10	
	.db $88, 3, %01000000, $18	

	.org $FFFA	; vecteur 	.org $1000	; Motif de background

	.dw NMI		; Interruption NMI (vblank). Adresse $FFFA
	.dw Start	; Interruption Reset (demarrage). Adresse $FFFC
	.dw 0		; Interruption logiciel (instruction BRK). Adresse $FFFE

;;;;; La prochaine section est une partie du ROM qui correspond aux adresse $0000 à $1FFF de la vram du PPU ;;;;;;

	.bank 2        ; change to bank 2
	.org $0000	; Motif de Sprite

SpritePM1:	; Tuile 0
	.db %00000111
	.db %00011111 
	.db %00111111
	.db %01111111
	.db %01111111
	.db %11111111
	.db %11111111
	.db %11111111

	.db %00000111 
	.db %00011000
	.db %00100000 
	.db %01000000
	.db %01000000
	.db %10000000
	.db %10000000
	.db %10000000

SpritePM2:	; Tuile 1
	.db %11100000 
	.db %11111000
	.db %11111100 
	.db %11111110
	.db %11111111
	.db %11111111
	.db %11100000
	.db %10000000 

	.db %11100000 
	.db %00011000
	.db %00000100 
	.db %00000010
	.db %00000001
	.db %00011111
	.db %01100000
	.db %10000000

SpriteF1:	; Tuile 2 ; fantome
	.db %00011111 
	.db %00111111
	.db %01100011 
	.db %11011101
	.db %11011101
	.db %11011101
	.db %11011101
	.db %11100011

	.db %00000000
	.db %00000000
	.db %00011100 
	.db %00111110
	.db %00111110
	.db %00111110
	.db %00111110
	.db %00011100

SpriteF2:	; Tuile 3
	.db %11111111 
	.db %11111111
	.db %01110111 
	.db %01110111
	.db %01110111
	.db %01111111
	.db %01111011
	.db %00110001

	.db %00000000 
	.db %00110000
	.db %00011111 
	.db %00001111
	.db %00001000
	.db %00000000
	.db %00000000
	.db %00000000

SpritePMHaut:	; Tuile 4 ;PacmanUp&Down
	.db %00001100 
	.db %00011100
	.db %00111100 
	.db %01111100
	.db %01111100
	.db %11111110
	.db %11111110
	.db %11111111

	.db %00001100 
	.db %00010100
	.db %00100100 
	.db %01000100
	.db %01000100
	.db %10000010
	.db %10000010
	.db %10000001

SpriteEyes:
	.db %00011111 
	.db %00111111
	.db %01100011 
	.db %11000001
	.db %11000001
	.db %11000001
	.db %11000001
	.db %11100011

	.db %00000000
	.db %00000000
	.db %00011100 
	.db %00111110
	.db %00111110
	.db %00111110
	.db %00111110
	.db %00011100

	.org $1000	; Motif de background
Background:	; Nous pouvons mettre autant de "Label" pour la même adresse.
BackgroundMotif10:	; Tuile 0
	.db %11111111
	.db %11111111
	.db %11111100 
	.db %00110000
	.db %00000011 
	.db %11001111
	.db %11111111 
	.db %11111111

	.db %00000000 
	.db %00000011
	.db %11001111 
	.db %11111111
	.db %11111111 
	.db %11111100
	.db %00110000 
	.db %00000000


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DirectionPacman: .ds 1
BouchePacman: .ds 1
BoucheTimer: .ds 1
BoucheVitesse: .ds 1
BoucheRapide: .ds 1
FantomeOnOff: .ds 1
DirectionFantome1: .ds 1
DirectionFantome2: .ds 1
DirectionFantome3: .ds 1
DirectionFantome4: .ds 1
SelectionFantome: .ds 1
