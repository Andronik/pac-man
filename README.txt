Pak-Man

Un petit programme en Assembleur 6502 (NES) basé sur Pac-Man de NAMCO qui permet le contrôle de Pak-Man et des 4 fantômes avec un deuxième joueur.

Pour compiler le code (pAKman.asm), on doit utiliser le programme nesasm. Le code compilé en .nes (pAKman.nes) est déjà inclut pour linux. 

Pour lancer le programme, l'émulateur de jeux NES fceux.exe est recommendé: http://www.fceux.com/web/home.html

Vérifiez les options de l'émulateur pour contrôler Pak-man (haut, bas, gauche et droite). Avec A/B on peut faire Pak-Man accélérer. 

Le deuxième joueur peut contrôler un fantôme à la fois et changer de fantôme avec A/B.
